Ubuntu Studio 16.04 Realtime Optimization
==========================================

Author: This Mächler
maechler@mm-computing.ch
2016-06-19

Inspiered by this article (thanks a lot, it's really very usefull to me!):
http://wiki.linuxaudio.org/wiki/system_configuration

Here my works to optimize Ubuntu Studio 16.04 to reach the lowest 
audio latencies possible are documented (I decided to not use a realtime-kernel; stability, updates, nvidia graphics..).

There is a script that applies optimizations ("optimize-realtime"):

- installs service "rtaudio-mode" that stops services that may cause xruns 
  and switches to  CPU frequency scaling governor "performance"
- removes some packages containing parts that may cause xruns
 

Please read HOWTO for further information.
